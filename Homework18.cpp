// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
// используем template для типа данных в массиве 
template <typename T>
//класс для реализации стэка 
class Stack {
private:
    T* value = nullptr; // это будет динамический массив с типом данных заданным template
    int size = 0; // размер стэка
public:     
    // добавления нового элемента в стэк
    // Так как у нас заранее неизвестно количество элементов в стэке, то при каждом добавлении элемента
    // создается массив на один элемент больше чем существующего, новое значение вставляется в нулевой элемент
    // А потом копируем все значения из существующего массива, в новый созданый и меняем указатель value на новый массив
    // старый массив уничтожаем
    void Push(T newvalue) 
    {
        T* newArray = new T[size + 1];
        newArray[0] = newvalue;
        int i = 1;
        for (T* p = &value[0]; p < value + size; p++)
        {
            newArray[i] = *p;
            i++;
        }
        size++;
        delete[] value;
        value = newArray;
    }
    // аналогично Push, только в данном случае создаем массив на один элмент меньше
    // в result сохраняем значение, элемента массива который надо вернуть
    T Pop()
    {
        T result = value[0];
        T* newArray = new T[size];
        int i = 0;
        for (T* p = &value[0]+1; p < value + size; p++)
        {
            newArray[i] = *p;
            i++;
        }
        size--;
        delete[] value;
        value = newArray;
        return result;
    }
    // вывод в консоль всех элементов стэка
    void ShowStack()
    {
        std::cout << "Stack: ";
        for (T* p = &value[0]; p < value + size; p++)
        {
            std::cout << *p;
        }
    }
};
;
int main()
{
    std::cout<< "Stack int" << '\n';
    Stack<int> s;
    s.Push(1);
    s.Push(2);
    s.Push(3);
    std::cout << '\n';
    s.ShowStack();
    std::cout << '\n';
    std::cout << "Pop: " <<  s.Pop() << '\n';
    s.ShowStack();
    std::cout << '\n' << "Pop: " << s.Pop() << '\n';
    s.ShowStack();
    s.Push(4);
    std::cout << '\n';
    s.ShowStack();

    std::cout << '\n' << '\n' << "Stack char" << '\n';
    Stack<char> s2;
    s2.Push('a');
    s2.Push('b');
    s2.Push('c');
    std::cout << '\n';
    s2.ShowStack();
    std::cout << '\n';
    std::cout << "Pop: " << s2.Pop() << '\n';
    s2.ShowStack();
    std::cout << '\n' << "Pop: " << s2.Pop() << '\n';
    s2.ShowStack();
    s2.Push('d');
    std::cout << '\n';
    s2.ShowStack();

    std::cout << '\n' << '\n' << "Stack string" << '\n';
    Stack<std::string> s3;
    s3.Push("aaa");
    s3.Push("bbb");
    s3.Push("ccc");
    std::cout << '\n';
    s3.ShowStack();
    std::cout << '\n';
    std::cout << "Pop: " << s3.Pop() << '\n';
    s3.ShowStack();
    std::cout << '\n' << "Pop: " << s3.Pop() << '\n';
    s3.ShowStack();
    s3.Push("ddd");
    std::cout << '\n';
    s3.ShowStack();
}

